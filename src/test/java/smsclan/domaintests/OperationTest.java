/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smsclan.domaintests;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import smsclan.domain.SMSClanManager;
import smsclan.domain.events.ClanEvent;
import smsclan.sms.SMSReceiver;
import smsclan.sms.SMSSender;

/**
 *
 * @author User
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = smsclan.app.SMSClannerApplication.class)
public class OperationTest {

    int cnt = 0;

    @Test
    public void testNewUserHasRegistered() {

        SMSClanManager clanManager = new SMSClanManager();

        SMSSender smsSender = createFakeSMSSender();
        FakeSMSReceiver smsReceiver = (FakeSMSReceiver) createFakeSMSReceiver();

        clanManager.setSMSSender(smsSender);
        clanManager.setSMSReceiver(smsReceiver);
        cnt = 0;
        clanManager.setClanEventsListener((ClanEvent e) -> {
            cnt++;
        });
        clanManager.signIn();

        smsReceiver.raiseNewMessageEvent("000", "REG ALFONS 11/22/2014 M METRO MANILA");
        assertTrue(cnt > 0);
    }

    private SMSSender createFakeSMSSender() {
        return null;
    }

    private SMSReceiver createFakeSMSReceiver() {
        return new FakeSMSReceiver();
    }
}
