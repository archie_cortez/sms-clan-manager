package smsclan.domaintests;

import static org.hamcrest.CoreMatchers.instanceOf;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import smsclan.domain.ClannerMessage;
import smsclan.domain.smscommands.Command;
import smsclan.domain.smscommands.SMSCommandParser;
import smsclan.domain.smscommands.RegistrationCommand;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = smsclan.app.SMSClannerApplication.class)
public class CommandParserTest {

    private ClannerMessage createFakeMessage(String from, String message) {
        return new ClannerMessage(from, message);
    }

    @Test
    public void contextLoads() {
    }

    @Test
    public void testRegisterCommand() {

        ClannerMessage m = createFakeMessage("000", "REG ALFONS 11/22/2014 M METRO MANILA");
        Command cmd = SMSCommandParser.parse(m);
        Assert.assertThat(cmd, instanceOf(RegistrationCommand.class));

        RegistrationCommand regCommand = (RegistrationCommand) cmd;

        Assert.assertEquals("REG", cmd.getCommand());
        Assert.assertEquals("ALFONS", regCommand.getAlias());
        Assert.assertEquals("11/22/2014", regCommand.getBirthday());
        Assert.assertEquals("M", regCommand.getSex());
        Assert.assertEquals("METRO MANILA", regCommand.getAddress());
        Assert.assertEquals("000", regCommand.getContactNumber());
    }
}
