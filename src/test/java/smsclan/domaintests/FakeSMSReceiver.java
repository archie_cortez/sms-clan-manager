/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smsclan.domaintests;

import smsclan.domain.ClannerMessage;
import smsclan.sms.SMSReceivedListener;
import smsclan.sms.SMSReceiver;

/**
 *
 * @author User
 */
public class FakeSMSReceiver implements SMSReceiver {

    private SMSReceivedListener listener;

    public FakeSMSReceiver() {

    }

    @Override
    public void setListener(SMSReceivedListener listener) {
        this.listener = listener;
    }

    @Override
    public void startReceiving() {

    }

    @Override
    public void stopReceiving() {

    }

    void raiseNewMessageEvent(String from, String message) {
        if (listener != null) {
            listener.newMessageReceived(new ClannerMessage(from, message));
        }
    }

}
