/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smsclan.sms;

import smsclan.domain.ClannerMessage;

/**
 *
 * @author User
 */
public interface SMSReceivedListener {

    void newMessageReceived(ClannerMessage message);
}
