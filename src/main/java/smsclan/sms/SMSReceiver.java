/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smsclan.sms;

/**
 *
 * @author User
 */
public interface SMSReceiver {

    void setListener(SMSReceivedListener listener);
    void startReceiving();
    void stopReceiving();
}
