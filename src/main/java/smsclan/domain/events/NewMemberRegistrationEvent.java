/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smsclan.domain.events;

import smsclan.domain.smscommands.RegistrationCommand;

/**
 *
 * @author User
 */
public class NewMemberRegistrationEvent extends ClanEvent {

    private RegistrationCommand registrationCommandInfo;

    public RegistrationCommand getRegistrationCommandInfo() {
        return registrationCommandInfo;
    }

    public NewMemberRegistrationEvent(RegistrationCommand command) {
        registrationCommandInfo = command;
    }

    public void setRegistrationCommandInfo(RegistrationCommand registrationCommandInfo) {
        this.registrationCommandInfo = registrationCommandInfo;
    }

}
