/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smsclan.domain.smscommands;

import smsclan.domain.SMSClanManager;

/**
 *
 * @author User
 */
public abstract class Command {

    private String command;

    public Command(String cmd) {
        command = cmd;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public abstract void execute(SMSClanManager context);
}
