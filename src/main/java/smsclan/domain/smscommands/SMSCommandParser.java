/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smsclan.domain.smscommands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import smsclan.domain.ClannerMessage;

/**
 *
 * @author User
 */
public class SMSCommandParser {

    public static Command parse(ClannerMessage msg) {

        String patternString = "([A-Z]+)\\s+([A-Z0-9]+)\\s+(\\d\\d/\\d\\d/\\d\\d\\d\\d)\\s+(M|F)\\s+(.*)";

        Pattern pattern = Pattern.compile(patternString);

        Matcher m = pattern.matcher(msg.getMessage());

        String cmd = null, alias = null, birthday = null, sex = null, address = null;

        while (m.find()) {
            cmd = m.group(1);
            alias = m.group(2);
            birthday = m.group(3);
            sex = m.group(4);
            address = m.group(5);
            break;
        }

        if (cmd != null && cmd.equals("REG")) {
            return new RegistrationCommand(cmd, alias, birthday, sex, address, msg.getFrom());
        } else {
            return null;
        }
    }
}
