/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smsclan.domain.smscommands;

import smsclan.domain.SMSClanManager;
import smsclan.domain.events.NewMemberRegistrationEvent;

/**
 *
 * @author User
 */
public class RegistrationCommand extends Command {

    private String alias;
    private String birthday;
    private String sex;
    private String address;
    private String contactNumber;

    RegistrationCommand(String cmd, String alias, String birthday,
            String sex, String address, String contactNumber) {
        super(cmd);

        this.alias = alias;
        this.birthday = birthday;
        this.sex = sex;
        this.address = address;
        this.contactNumber = contactNumber;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    @Override
    public void execute(SMSClanManager context) {
        //TODO Save to db here
        context.fireClanEvent(new NewMemberRegistrationEvent(this));
    }

}
