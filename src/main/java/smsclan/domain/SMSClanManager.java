/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smsclan.domain;

import smsclan.domain.events.ClanEvent;
import smsclan.domain.events.NewMemberRegistrationEvent;
import smsclan.domain.smscommands.Command;
import smsclan.domain.smscommands.SMSCommandParser;
import smsclan.sms.SMSReceivedListener;
import smsclan.sms.SMSReceiver;
import smsclan.sms.SMSSender;
import smsclan.domain.events.ClanEventListener;

/**
 *
 * @author User
 */
public class SMSClanManager implements SMSReceivedListener {

    private SMSSender smsSenderService;
    private SMSReceiver smsReceiver;
    private ClanEventListener listener;

    public void signIn() {
        smsReceiver.startReceiving();
    }

    public void signOut() {
        smsReceiver.stopReceiving();
    }

    public void setSMSSender(SMSSender smsSenderService) {
        this.smsSenderService = smsSenderService;
    }

    public void setSMSReceiver(SMSReceiver smsReceiver) {
        this.smsReceiver = smsReceiver;
        smsReceiver.setListener(this);
    }

    @Override
    public void newMessageReceived(ClannerMessage message) {
        Command cmd = SMSCommandParser.parse(message);
        if (cmd != null) {
            cmd.execute(this);
        }
    }

    public void setClanEventsListener(ClanEventListener eventListener) {
        this.listener = eventListener;
    }

    public void fireClanEvent(ClanEvent clanEvent) {
        if (listener != null) {
            listener.clanEventOccured(clanEvent);
        }
    }
}
