package smsclan.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SMSClannerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SMSClannerApplication.class, args);
    }
}
